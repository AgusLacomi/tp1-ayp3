#include <stdio.h>

//By AgusLacomi
int main()
{
    int enteros[3];

    for (int i = 0; i < 3; i++)
    {
        printf("Ingrese el %d valor: ", (i + 1));
        scanf("%d", &enteros[i]);
    }

    float promedio = (enteros[0] + enteros[1] + enteros[2]) / 3;

    printf("El promedio de los valores ingresados es: %f", promedio);

    return 0;
}
