#include <stdio.h>

//by AgusLacomi
int main()
{
    int opcion;

    do
    {
        printf("\n¡MENU! \n");
        printf("1. Opcion 1 \n");
        printf("2. Opcion 2 \n");
        printf("3. Opcion 3 \n");
        printf("4. Salir \n");

        printf("Su opcion es: ");
        scanf("%i", &opcion);

        switch (opcion)
        {
        case 1:
            printf("\nCon el dedo índice, señalo al cielo,\nUno es un número, es el primero y el más leal.\n");
            break;
        case 2:
            printf("\nJuntos estamos, lado a lado,\nDos es nuestro número, nunca separados.\n");
            break;
        case 3:
            printf("\nTres pequeños cerditos, cada uno con su casa,\nUno de paja, otro de madera, el tercero de ladrillo, para que nada pase.\n");
            break;
        case 4:
            printf("\nSe está yendo, se está despidiendo, del menú se va, poco a poco desapareciendo.\n");
            break;
        default:
            printf("\nNo es una opción, eso está claro,\nEn la lista no aparece, ni siquiera por un rato.\n");
            break;
        }
    } while (opcion != 4);

    return 0;
}
