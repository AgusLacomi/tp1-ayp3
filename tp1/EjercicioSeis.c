#include <stdio.h>

//By AgusLacomi
int main()
{

    int numero;

    printf("Ingrese un número: ");
    scanf("%i", &numero);

    if (numero % 2 == 0)
    {
        printf("%i es par", numero);
    }
    else
    {
        printf("%i no es par", numero);
    }

    return 0;
}
