#include <stdio.h>

// By AgusLacomi
int main()
{
    int lista[10] = {1,
                     3,
                     5,
                     12,
                     4,
                     9,
                     7,
                     10,
                     4,
                     1};

    int max = lista[0];

    int tamañoDeLista = sizeof(lista) / sizeof(int);

    for (int i = 1; i < tamañoDeLista; i++)
    {
        if (max < lista[i])
        {
            max = lista[i];
        }
    }

    printf("El mayor de la lista es: %d", max);

    return 0;
}
