/*
Parte 1:
Hacer la implementación de una lista enlazada.
Debe haber funciones para:
-Crear e inicializar la lista
-Agregar un elemento
-Obtener el largo de la lista
-Obtener un elemento N de la lista
-Eliminar un elemento N de la lista
-Imprimir la lista

Parte 2:
Implementar una lista de enteros ordenada. Cada elemento que agrego queda ordenado en la lista, de manera que al imprimirla se imprime automáticamente ordenada.*/

#include <stdio.h>
#include <malloc.h>

typedef struct Node
{
    int value;
    struct Node *next;
} Node;

typedef struct List
{
    struct Node *head;
    struct Node *tail;
    int size; // Para que sea de Orden O(1)

} List;

Node *NewNode()
{
    Node *nodo = malloc(sizeof(Node));
    nodo->next = NULL;
    return nodo;
}

List *NewList()
{
    List *lista = malloc(sizeof(List));
    lista->head = NULL;
    lista->tail = NULL;
    lista->size = 0;
    return lista;
}

// Para que sea de Orden O(1)
int SizeOf(List *lista)
{
    return lista->size;
}

// Agrega al final de la lista el elemento ingresado
void Add(List *lista, int v)
{
    Node *nodoNuevo = NewNode();
    nodoNuevo->value = v;

    if (lista->head == NULL)
    {
        lista->head = nodoNuevo;
        lista->size++;
    }
    else if (lista->tail == NULL)
    {
        lista->tail = nodoNuevo;
        lista->head->next = lista->tail;
        lista->size++;
    }
    else
    {
        lista->tail->next = nodoNuevo;
        lista->tail = lista->tail->next;
        lista->size++;
    }
}

int Get(List *lista, int posicion)
{
    if (posicion < 0 || posicion > SizeOf(lista))
    {
        return 0;
    }
    Node *current = lista->head;
    while (current != NULL && posicion > 0)
    {
        current = current->next;
        posicion--;
    }
    return current->value;
}

void Remove(List *lista, int posicion)
{
    if (posicion < 0 || posicion > SizeOf(lista))
    {
        return;
    }

    Node *current = lista->head;

    while (current != NULL && posicion > 1)
    {
        current = current->next;
        posicion--;
    }
    current->next = current->next->next;
    lista->size--;
}

void PrintList(List *lista)
{
    Node *current = lista->head;
    int i = 0;
    printf("Arreglo: [ ");
    while (i < SizeOf(lista))
    {
        printf("%d ", current->value);
        current = current->next;
        i++;
    }
    printf("]");
    printf("\n");
}

void AddOrdenado(List *l, int v)
{
    Node *nodo = NewNode();
    nodo->value = v;

    if (l->head == NULL)
    {
        l->head = nodo;
        l->tail = nodo;
        l->size++;
        return;
    }
    if (nodo->value < l->head->value)
    {
        nodo->next = l->head;
        l->head = nodo;
        l->size++;
        return;
    }
    if (nodo->value > l->tail->value)
    {
        l->tail->next = nodo;
        l->tail = nodo;
        l->size++;
        return;
    }
    Node *current = l->head;
    while (current->next != NULL && current->next->value < v)
    {
        current = current->next;
    }
    nodo->next = current->next;
    current->next = nodo;
}

int main()
{
    List *lista = NewList();

    return 0;
}
