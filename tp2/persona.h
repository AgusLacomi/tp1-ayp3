// crear una entidad persona con todos los atributos necesarios para definir esta entidad de negocio (a elección). Crear funciones que permitan crear la entidad, modificar sus datos, imprimir la información de la entidad. Interpretar qué sucede a nivel memoria cuando llamamos a una función y le pasamos el struct

#define len 50

typedef struct humano
{
    char nombre[len];
    char apellido[len];
    int edad;
    int dni;
} cliente;